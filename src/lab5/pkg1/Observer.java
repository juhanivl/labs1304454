/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5.pkg1;

/**
 *
 * @author Juhani
 */
public interface Observer {
    
    public void newMessage(String message);
    
}
