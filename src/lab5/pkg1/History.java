package lab5.pkg1;

import java.util.ArrayList;

public class History {

    private static final History Instance = new History();

    ArrayList<String> history = new ArrayList<String>();
    ArrayList<Observer> observers = new ArrayList<Observer>();

    private History() {
    }

    public static History getInstance() {
        return Instance;
    }

    //All history methods
    //Add to history
    public void addMessage(String message) {
        history.add(message);
        for (Observer ob : observers) {
            ob.newMessage(message);
        }
    }

    public String showHistory() {
        return history.get(history.size() - 1);
    }

    public void addObserver(Observer ob) {
        observers.add(ob);
    }

}
