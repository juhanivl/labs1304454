package lab5.pkg1;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Interpreter implements Runnable, Observer {
    private Socket connection;
    private String message; 
    private PrintWriter output;  //Communicate via input/output-stream. Output goes away from you and input towards
    private BufferedReader input;
    
    //CONSTRUCTOR
    public Interpreter  (Socket connection){
        this.connection = connection;
        this.message = message;
    }
    
    public void run(){
    while(true){
    try{ 
        setUpStream();       //set up connection
        whileChatting();     //runs the program while you're chatting
        }catch(IOException ioException){
    
    }
    }
    }
    //SETTING UP STREAM
    private void setUpStream() throws IOException{
        output = new PrintWriter(connection.getOutputStream()); //stream is a way you communicate with another computer
        output.flush(); //To flush out redundant bytes. You can only flush your computer
        input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        System.out.println("Streams are now setup!");
    }
    
    //WHILE CHATTING
    private void whileChatting() throws IOException{
        output.println("You are now connected");
        do{
           message = (String) input.readLine();
           History myHistory=History.getInstance();
           myHistory.addMessage(message);
           System.out.println(message);
            
        }while(!message.equals("CLIENT - END"));//Have a convo until other types END
    }
        
           
                
                //send a message to client
        /*private void sendMessage(String message){
                History myHistory=History.getInstance();
                myHistory.showHistory();
                output.println("Juhani - " + message);
                output.println(myHistory.showHistory());
        }*/

    public void newMessage(String message) {
       output.println(connection.getInetAddress() + message);
       output.flush();
    }
       
        
            
        
        
        
        
}
