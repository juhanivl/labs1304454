package lab5.pkg1;
import java.io.*;
import java.net.*;

public class Server {
    private ServerSocket server;
    private Socket connection;   
    private PrintWriter output;  //Communicate via input/output-stream. Output goes away from you and input towards
    private BufferedReader input;
    //10.112.202.208, 6789
    public void startRunning(){
        try{
           server = new ServerSocket(1903,100); //int port and int backlog
           while(true){
               try{ 
                   //connect and have conversation  
                   waitForConnection(); //wait foo someone to connect with
                   Interpreter interpreter = new Interpreter(connection);
                   History myHistory=History.getInstance();
                   Thread thread = new Thread(interpreter);
                   myHistory.addObserver(interpreter);
                   thread.start();
                   
               }catch(EOFException eofException){
                   System.out.println("Server ended the connection!");
           }
           }    
        }
           catch(IOException ioException){
            ioException.printStackTrace();
        }
    }
    
    //MEHTODS
    //wait for connection, then display conneciton information
    private void waitForConnection() throws IOException{
        System.out.println("Waiting for someone to connect...");
        connection = server.accept();   //does this all over and over again
        System.out.println("Now connected to " + connection.getInetAddress().getHostName()); //the address where the socket is 
    }
    
    //close crap-method when convo ends = housekeeping
    private void closeCrap(){
        System.out.println("Closing connection...");
        try{
            output.close();     //closes connection to them
            input.close();      //conneciton from them
            connection.close(); //closes the socket =the main connest
            
        }catch(IOException ioException){
            
            ioException.printStackTrace();
        }
    }
}
