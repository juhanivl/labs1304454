//CHECK report file in the package to see the results
package lab5.pkg3;

/**
 *
 * @author Juhani
 */
public class Main {

    public static void main(String[] args) {
       //ARRAY
       /*
       ArrayLista myArrayList = new ArrayLista();
       
       long startTime1=System.currentTimeMillis();
       myArrayList.täytäArrayList();
       myArrayList.sortArrayList();
       long endTime1=System.currentTimeMillis();
       long difference1= endTime1-startTime1;
       System.out.println("Fill and sort time: " + difference1);
       
       long startTime2=System.currentTimeMillis();
       myArrayList.hundrethItem();
       long endTime2=System.currentTimeMillis();
       long difference2= endTime2-startTime2;
       System.out.println("Get 100th time: " + difference2);
       */

       //HASHSET
       HashSetti myHashSet = new HashSetti();
       
       long startTime1=System.currentTimeMillis();
       myHashSet.täytäHashSet();
       long endTime1=System.currentTimeMillis();
       long difference1= endTime1-startTime1;
       System.out.println("Fill and sort time: " + difference1);
       
       long startTime2=System.currentTimeMillis();
       myHashSet.hundrethItem();
       long endTime2=System.currentTimeMillis();
       long difference2= endTime2-startTime2;
       System.out.println("Get 100th time: " + difference2);
       
       
       //TREESET
        /*
       TreeSetti myTreeSet = new TreeSetti();
       
       long startTime=System.currentTimeMillis();
       myTreeSet.täytäTreeSet();
       long endTime=System.currentTimeMillis();
       long difference= endTime-startTime;
       System.out.println("Fill and sort time: " + difference);
       
       long startTime3=System.currentTimeMillis();
       myTreeSet.hundrethItem();
       long endTime3=System.currentTimeMillis();
       long difference3=endTime3-startTime3;
       System.out.println("Getting the 100th item: " + difference3);
       */
    
    }
}
