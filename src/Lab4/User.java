package Lab4;


import java.util.Scanner;

public class User {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        WaterDispenser wd1 = new WaterDispenser();
        wd1.valveControl();

        System.out.println("WATER DISPENSER +");
        System.out.println("1 : water");
        System.out.println("2 : valve control");
        System.out.println("quit : quit");
        
        
        while (true) {
            System.out.print("Input: ");
            String input = reader.nextLine();
            if (input.equals("quit")) {
                break;
            } else if (input.equals("1")) {
                System.out.println("How much water do you want: ");
                int amount = Integer.parseInt(reader.nextLine());
                wd1.setAmount(amount);               
                wd1.dispenseWater();
            } else if (input.equals("2")) {
                wd1.valveControl();
                System.out.println("How much water do you want: ");
                int amount = Integer.parseInt(reader.nextLine());
                wd1.setAmount(amount);
                wd1.valveControl();
                wd1.dispenseWater();
            }
        }

    }
}
