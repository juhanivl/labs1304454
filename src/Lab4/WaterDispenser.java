package Lab4;


import java.io.IOException;
import java.util.Scanner;

public class WaterDispenser implements Runnable {

    private boolean valveClosed;
    private int waterLevel;
    private int amount;
    private Thread waterRunning;

    //CONSTRUCTOR
    WaterDispenser() {
        this.valveClosed = true;
        this.waterLevel = 100;
        this.amount = 0;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    
    
    public void valveControl() {
        if (this.valveClosed == true) {
            this.valveClosed = false;
            System.out.println("Valve is open!");
        } else if (this.valveClosed == false) {
            this.valveClosed = true;
            System.out.println("Valve is closed!");
        } else {
            System.out.println("ERROR");
        }
    }

    @Override
    public void run() {
        while (!this.valveClosed && this.amount != 0) {
            if (this.waterLevel >= 1) {
                this.waterLevel += -1;
                this.amount += -1;
                System.out.print("* ");
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (this.amount == 0) {
                    System.out.println("No more water");
                    break;
                }
            } else {
                System.out.println("Not enough water!");
                break;
            }
        }
    }

    

    public void dispenseWater() {
        this.waterRunning = new Thread(this);
        this.waterRunning.start();
        
    }

    public void printWater() {
        for (int i = 0; i < amount; i++) {
            try {
                Thread.sleep(1000);//1000 milliseconds is one second.
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("*");
            waterLevel = waterLevel - 1;
            if (valveClosed == true) {
                break;
            }
        }
    }

}
