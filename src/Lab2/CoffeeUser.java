package Lab2;


import java.util.Scanner;
        
public class CoffeeUser {

    public static void main(String[] args) {
        
        
        Scanner reader = new Scanner(System.in);
        CoffeeDispenser coffee1=new CoffeeDispenser();
        
        System.out.println("COFFEEMAKER 2000");
        System.out.println("press any key to continue press q to quit.");
        
        while (true) {
        String command=reader.nextLine();
        if (command.equals("q")) {
        break;
        }
       

        System.out.println("press p to turn the coffeemaker on/off.");
        System.out.println("press c to have a coffee.");
        System.out.println("press e to have a espresso.");
        System.out.println("press s to show status.");
        System.out.println("press q to quit.");
        System.out.println("WRITE A COMMAND:");
       
       if (command.equals("p")){
           coffee1.pressOnOff();
       }
       else if(command.equals("s")){
           coffee1.status();
       }
       else if(command.equals("c")){
           coffee1.coffee();
       }
       else if(command.equals("e")){
           coffee1.espresso();
       }

    }
    
}
}