
package Lab1;

public class WaterDispenser {
    
//muuttujat
    private int waterLevel;
    private boolean coverOn;
    private boolean powerOn;
    
    //konstruktori
        public void waterDispenser(int waterLevel, boolean coverOn, boolean powerOn) {
        this.waterLevel = 100;
        this.coverOn = true;
        this.powerOn = true;
    }
    
    
 //methodit
    public void fillTank(){
        if (waterLevel<100){
            this.waterLevel=100;
            System.out.println("Water tank is filled");
        }
    }

    public void pressOnOff(){
        if(this.powerOn=true){
        this.powerOn=false;
            System.out.println("Power off");
        }
        else{
            this.powerOn=true;
            System.out.println("Power is on");
        }
    }
    
    public void pressWaterButton(){
        if (powerOn=true && this.waterLevel >=1){
            this.waterLevel += -1;
            System.out.println("Water is coming out");
        }
        else{
            System.out.println("No water...fail.");
        }
    }
    
    
    
    
}