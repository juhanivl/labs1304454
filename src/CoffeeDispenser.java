public class CoffeeDispenser {
//You can turn the machine on and off
//User can press espresso to make an espresso
//User can press regular to make a regular coffee
//Water tank can be filled Beans tank can be filled
//Coffee maker rinses itself when it is set on and again when it is set off.
//Cleaning after x times of use

//MUUTTUJAT
    private int waterLevel;
    private int coffeeBeans;
    private int counter;
    private int fillW;
    private int fillC;
    private boolean powerOn;
    
//KONSTRUKTORI
    public CoffeeDispenser() {
        this.waterLevel = 3000;
        this.coffeeBeans = 300;
        this.fillW=0;
        this.fillC=0;
        this.counter = 0;
        this.powerOn = true;
    }

public void pressOnOff(){
        if(this.powerOn==true){
        this.powerOn=false;
            System.out.println("Power is off");
        }
        else{
            this.powerOn=true;
            System.out.println("Power is on");
            //RINSE
            waterLevel=waterLevel-1;
        }
    }

public void espresso(){
    coffeeBeans=coffeeBeans-8;
    waterLevel=waterLevel-30;
    System.out.println("Espresso coming up!");
    counter++;
    if(counter<10){
        System.out.println("No need for cleaning.");
    }
    else{
        clean();
        
    }
    if (waterLevel<0){
        fillWater();
    }
    if(coffeeBeans<0){
        fillCoffee();
    }
    
}
public void coffee(){
    coffeeBeans=coffeeBeans-10;
    waterLevel=waterLevel-150;
    System.out.println("Coffee coming up!");
    counter++;
    if(counter<10){
        System.out.println("No need for cleaning.");
    }
    else{
        clean();
    }
    if(waterLevel<0){
        fillWater();
    }
    if(coffeeBeans<0){
        fillCoffee();
    }
    
    
}

public void fillWater(){
    fillW=3000-waterLevel;
    System.out.println(fillW + " ml water used.");
    waterLevel=fillW+waterLevel;
    System.out.println("Water level filled. " + waterLevel);
    
}

public void fillCoffee(){
    fillC=300-coffeeBeans;
    System.out.println(fillC + " g coffee used.");
    coffeeBeans=fillC+coffeeBeans;
    System.out.println("Coffee beans filled" + coffeeBeans);
}

public void status(){
    System.out.println("Water level: " + waterLevel);
    System.out.println("Coffee beans " + coffeeBeans);
    System.out.println("Power status: " + powerOn);
    System.out.println("Counter: " + counter);
}

public void clean(){
    waterLevel=waterLevel-300;
    System.out.println("I cleaned myself!");
    counter=0;
}


}