package Lab3;

import java.util.ArrayList;
import java.util.Scanner;

public class Cd {
    Scanner reader = new Scanner(System.in);
    int number = 0;
    int current= 0;
    String remove;
    String check;
    String stop;
    ArrayList<String> cd = new ArrayList<String>();

    public void addSongs(){
    
    while(true){
    System.out.println("Add songs by typing the name. Type stop to stop adding songs.");
    String add=reader.next();
    
        if (add.equals("stop")){
        break;
        }
        //ADDING TRACK
        System.out.println("Added song: " + add);
        cd.add(add);
        
    }
    }
    
    public void numberOfSongs(){
    System.out.println("the number of songs " + cd.size() );
    }
    
    public void showSong(){
        System.out.println("which song would you like to see: ");
        int number=reader.nextInt();
        System.out.println("song is: " + cd.get(number));
    }
    
    public void removeSong(){
        System.out.println("which song would you like to remove");
        String remove = reader.next();
    cd.remove(remove);
    }
    
    public void checkSong(){
        System.out.println("What song would you like to see");
        String check=reader.next();
    if (cd.contains(check)) {
        System.out.println("Song is on the track list");
    } else {
        System.out.println("Song is not on the track list");
    }
    }
    
    public void playSongs(){
    System.out.println("Playing track " + cd.get(number));
    }
    
    public void nextSong(){
        number=number+1;
        if (number >= cd.size()){
            System.out.println("No more tracks"); 
            number=0;
        }
        else{
        System.out.println("Playing track " + cd.get(number));
        }
    }
    
    public void previousSong(){
        number=number-1;
         if (number<0){
            System.out.println("No songs before: " + cd.get(0));
            number=0;
        }
        else{
        System.out.println("Playing track " + cd.get(number));
        }
    }
}
